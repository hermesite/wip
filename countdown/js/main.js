var pi2 = 2 * Math.PI;
var width;
var midWidth;
var height;
var midHeight;
var radiusIn;
var midRadiusIn;
var radiusOut;
var midRadiusOut;

var data; // a global

var timeNow = new Date();

// Marcamos los días de fin de proyecto en el calendario
var projectDatesArray = new Array();
projectDatesArray.push(new Date());

// Generamos el objeto para la leyenda
var stats = {};
var projectNamesArray = {};

var timePosition;
var timeLeft = 0;
var timeCenter = 0;

d3.json("js/projects.json", function(error, json) {
    if (error) return console.warn(error);
    data = json;

    for (var i = data.projects.length - 1; i >= 0; i--) {

        var startDate = createDate(data.projects[i].start_date); //define start date for clock
        var endDate = createDate(data.projects[i].end_date); //define end date for

        if (timeNow > endDate) {} else {
            if (timeNow > startDate) {
                timeCenter = i;
                timeLeft = timeCenter - 1;
                timeRight = timeCenter + 1;
            } else {}
        }
    }

    populate(timeCenter, "current");
    populate(timeLeft, "passed");
    populate(timeRight, "coming");

});

function populate(timeSelection, timeTag) {

    d3.json("js/projects.json", function(error, json) {

        if (error) return console.warn(error);
        data = json;

        for (var i = data.projects.length - 1; i >= 0; i--) {

            var projectYear = (data.projects[i].end_date).substring(0, 4);
            var projectMonth = (data.projects[i].end_date).substring(5, 7);
            var projectDay = (data.projects[i].end_date).substring(8, 10);

            projectDatesArray.push(new Date(projectYear, projectMonth - 1, projectDay));

            projectNamesArray[data.projects[i].id] = data.projects[i].name;

            stats[Math.round(new Date(data.projects[i].end_date) / 1000)] = data.projects[i].color_value;
            stats[Math.round(new Date(data.projects[i].left_end_date) / 1000)] = data.projects[i].color_value;
            stats[Math.round(new Date(data.projects[i].right_end_date) / 1000)] = data.projects[i].color_value;


            if (i === timeSelection) {
                console.log(timeSelection + " - " + i);
                visualizeIt(
                    data.projects[i].id,
                    data.projects[i].name,
                    data.projects[i].start_date,
                    data.projects[i].end_date,
                    i,
                    timeTag);
            }

        };

    });
}

function visualizeIt(
    id,
    name,
    projectStartDate,
    projectEndDate,
    position,
    timeTag) {

    var startDate = createDate(projectStartDate); //define start date for clock
    var endDate = createDate(projectEndDate); //define end date for


    var projectYear = projectEndDate.substring(0, 4);
    var projectMonth = projectEndDate.substring(5, 7);
    var projectDay = projectEndDate.substring(8, 10);

    var mainGraphContainer = id + "-main-graph";
    var mainGraphTextStarting = id + "-starting-text";
    var mainGraphTextRemaning = id + "-remaning-text";

    // Scalfolding for widget


    var graphContainer = d3.select(".countdown-container")
        .append("div")
        .attr("id", function() {
            return position;
        })
        .attr("class", function(d, i) {
            if (timeTag == 'current') {
                return 'col-xs-12 col-sm-4';
            } else if (timeTag == 'passed') {
                return 'col-xs-12 col-sm-4 first-xs';
            } else {
                return 'col-xs-12 col-sm-4 last-xs';
            }
        })
        .append("div")
        .attr("class", function() {
            if (timeTag == 'current') {
                return 'panel panel-primary';
            } else if (timeTag == 'passed') {
                return 'panel panel-default';
            } else {
                return 'panel panel-default';
            }
        });

    // var headerGraph = graphContainer.append("div")
    //     .attr("class", "panel-heading");

    var bodyGraph = graphContainer.append("div")
        .attr("class", "panel-body");

    // var footerGraph = graphContainer.append("div")
    //     .attr("class", "panel-footer");

    // headerGraph.append('h3')
    //     .attr('class', function() {
    //         return 'panel-title';
    //     })
    //     .text(function(d) {
    //         if (timeTag == 'current') {
    //             return 'Work in progress';
    //         } else if (timeTag == 'passed') {
    //             return 'Done';
    //         } else {
    //             return 'To do';
    //         }
    //     });


    bodyGraph.append('h5')
        .append('span')
        .attr('class', 'label label-default')
        .text(remainingDays(startDate, endDate) + ' total days');

    bodyGraph.append('h2')
        .text(name);

    bodyGraph.append("h4")
        .attr("id", mainGraphTextStarting)
        .attr("class", function() {
            return "date-block";
        });

    bodyGraph.append("div")
        .attr("id", mainGraphContainer)
        .attr("class", "clock-big");

    var width = 150; //define clock width
    var midWidth = width / 2; //define clock height
    var height = 150; //define clock height
    var midHeight = height / 2; //define clock height
    var radiusIn = height / 2;
    var midRadiusIn = radiusIn / 2;
    var radiusOut = height / 2 - 10;
    var midRadiusOut = radiusOut / 2;


    createClock(startDate, endDate, width, height, radiusIn, radiusOut, mainGraphTextStarting, mainGraphTextRemaning, "2em");
}

function createDate(strDate) {
    var format = d3.time.format("%Y-%m-%d %H:%M:%S");
    return format.parse(strDate);
}

function remainingDays(current, target) {
    return Math.floor((target - current) / 1000 / 60 / 60 / 24);
}

function remainingHours(current, target) {
    return Math.floor((target - current) / 1000 / 60 / 60) % 24;
}

function remainingMinutes(current, target) {
    return Math.floor((target - current) / 1000 / 60) % 60;
}

function remainingSeconds(current, target) {
    return Math.floor((target - current) / 1000) % 60;
}

function createClock(
    start,
    end,
    width,
    height,
    radiusIn,
    radiusOut,
    clockTextStarting,
    clockTextRemaning,
    lineHeight) {

    function updateClock() {

        timeNow = new Date();

        if (end < timeNow) {

            document.getElementById(clockTextStarting).innerHTML =
                'has passed ' + remainingDays(end, timeNow) + ' days ago';

        } else {
            if (start < timeNow) {
                document.getElementById(clockTextStarting).innerHTML =
                    'will finish in <div class="row clock-container"><div class="col col-xs-3"><h1>' + remainingDays(timeNow, end) + '</h1><span class="measure">Days</span></div><div class="col col-xs-3"><h1>' + remainingHours(timeNow, end) + '</h1><span class="measure">Hours</span> </div><div class="col col-xs-3"><h1>' + remainingMinutes(timeNow, end) + '</h1><span class="measure">Minutes</span></div><div class="col col-xs-3"><h1>' + remainingSeconds(timeNow, end) + '</h1><span class="measure">Seconds</span></div></div>';
            } else {
                document.getElementById(clockTextStarting).innerHTML =
                    "is starting the " + convertDate(start);
            }
        }



    }

    setInterval(updateClock, 1000);

}

Date.prototype.getWeek = function() {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}

var today = new Date();
var weekNumber = today.getWeek();
var monthNumber = today.getMonth();
var yearNumber = today.getFullYear();
console.log(weekNumber); // Returns the week number as an integer
console.log(monthNumber); // Returns the week number as an integer
console.log(yearNumber); // Returns the week number as an integer

function convertDate(inputFormat) {
    function pad(s) {
        return (s < 10) ? '0' + s : s;
    }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}

function convertDateMe(inputFormat) {
    function pad(s) {
        return (s < 10) ? '0' + s : s;
    }
    var d = new Date(inputFormat);
    return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('-');
}