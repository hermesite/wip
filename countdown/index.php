<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Marketplaces Delivery Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
        echo '<link rel="shortcut icon" href="favicon.ico?t=' . time() . '" />';
    ?>
    <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Open+Sans:300,600,700" rel="stylesheet">
    <link rel="stylesheet" href="../components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../components/flexboxgrid/css/flexboxgrid.min.css">
    <link rel="stylesheet" href="../components/colorbrewer/colorbrewer.css">
    <link rel="stylesheet" href="../components/cal-heatmap/cal-heatmap.css">
    <link rel="stylesheet" href="css/main.css">

</head>

<body>
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="container-fluid">
        
    <div class="countdown-container row middle-xs"></div>
    </div>

    <!-- /container -->

    <script src="../components/jquery/dist/jquery.min.js"></script>
    <script src="../components/d3/d3.min.js"></script>
    <script src="../components/cal-heatmap/cal-heatmap.min.js"></script>

    <script src="js/main.js"></script>

</body>

</html>