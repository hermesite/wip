var tdviz = tdviz || {'version':0.1, 'controller':{}, 'viz': {} ,'extras': {} };

tdviz.viz.bcnRT =  function (options)
{

    // Object

    var self = {};

    // Global vars
    // Tetuan: 41.394872,2.175593
    // Glories: 41.403403, 2.187052
    // Corsega/S.Juan: 41.40189,2.166324

    self.originLatitude = 41.40189;
	self.originLongitude = 2.166324;

    // Get options data

    for (key in options){
        self[key] = options[key];
    }

    self.parentSelect = "#"+self.idName;

    self.init = function()
    {


        // svg init

        self.myLog("Iniciando network diagram... en ",3);
        self.myLog(self.parentSelect,3);
        self.svg = d3.select(self.parentSelect).append("svg")
            .attr("width",self.width)
            .attr("height",self.height)
            .call(d3.behavior.zoom().on("zoom", self.redraw))
            .append("g");

        // projection and path info


	    self.projection = d3.geo.mercator()
            .rotate([(0 - self.originLongitude), (0 - self.originLatitude), -45])
            .scale(448500)
            .translate([self.width/2 + 7, self.height/2]);

	    self.path = d3.geo.path()
	       .projection(self.projection);

        var leftDisplacement = self.width/2 + 215;
        var topDisplacement = self.height/2 + 88;

        // d3.json("js/3rdparty/world-110m.json", function(error, world) {
        // self.svg.insert("path", ".graticule")
        //     .datum(topojson.object(world, world.objects.land))
        //     .attr("class", "land")
        //     .attr("d", self.path)
        //     .attr("transform", function(){
        //         return "translate("+(leftDisplacement)+","+(topDisplacement)+") rotate(-45)"
        //     });
        // });


        self.bcnLatlong = [];
	    self.bcnPoints = [];

	    self.markPointEspanya = [];
	    self.markPointGlories = [];

//	for (var key in bcnrt) {
//	    bcnLatlong.push([+bcnrt[key].geo.info.lng, +bcnrt[key].geo.info.lat]);
//	    bcnPoints.push(projection([+bcnrt[key].geo.info.lng, +bcnrt[key].geo.info.lat]));
//	}

   //      self.markPointEspanya.push(self.projection([2.149136, 41.375005]));
   //      self.markPointGlories.push(self.projection([2.187052, 41.403403]));


   //       self.infoPointEspanya = self.svg.append("g").append("path")
			// .attr("d", d3.svg.symbol()
			// .size( function(d) { return 50; })
			// .type( function(d) { return d3.svg.symbolTypes[1]; }))
			// .attr("transform", "translate("+self.markPointEspanya[0][0]+", "+self.markPointEspanya[0][1]+")")
			// .style("fill", "red");

   //      self.infoPointGlories = self.svg.append("g").append("path")
			// .attr("d", d3.svg.symbol()
			// .size( function(d) { return 50; })
			// .type( function(d) { return d3.svg.symbolTypes[1]; }))
			// .attr("transform", "translate("+self.markPointGlories[0][0]+", "+self.markPointGlories[0][1]+")")
			// .style("fill", "red");


        self.legendSVG = d3.select("#"+self.legendId).append("svg")
            .attr("width",100)
            .attr("height",150)
            .append("g");


        for (var i in self.scaleType.domain())
        {

            var company = self.scaleType.domain()[i];

//            console.log(company);

            self.legendSVG
                .append("circle")
                .attr("class","legendNodes")
                .style("fill",self.scaleType(company))
                .attr("cx",10)
                .attr("cy",30+(i*25))
                .attr("r",5);

            self.legendSVG
                .append("text")
                .attr("class","legendTexts")
                .attr("x",20)
                .attr("y",34+(i*25))
                .text(company);
        }

        // warning message

        self.warningMessage = self.svg.append("text")
            .attr("text-anchor", "middle")
            .attr("class","netChartTextWarning")
            .attr("x", self.width/2)
            .attr("y",self.height/2)
            .text(self.loadingMessage);



    }

    self.render = function(data,dataIn)
    {
        console.log("REFRESHHHHHHHCANDO");
        console.log("En el render....");

        self.data = data;

        self.dataIn = dataIn;

//        d3.selectAll(".circleDraw").transition().duration(self.transTime).style("opacity",0.1).attr("r",0).remove();
//        d3.selectAll(".lineDraw").transition().duration(self.transTime).style("opacity",0.1).attr("r",0).remove();


        d3.selectAll(".circleDraw").remove();
        d3.selectAll(".lineDraw").remove();

        if(self.dataIn=='bicing' || self.dataIn=='all')
        {

            console.log("BICING");

            self.drawPoints = [];
            self.dataPoints = [];


            for(var i in self.data)
            {
                var point = self.data[i];

                if(point.type=='bicing')
                {

                    self.drawPoints.push(self.projection([point.geo.info.lng, point.geo.info.lat]));
                    self.dataPoints.push(point);


                    self.points = self.svg.selectAll("circle.bicing")
                                .data(self.dataPoints)
                                .enter().append("svg:circle")
                                    .attr("cx", function(d,j) {
                                            return Math.floor(self.drawPoints[j][0]);
                                    })
                                    .attr("cy", function(d,j) {

                                            return Math.floor(self.drawPoints[j][1]);
                                    })
                        .attr("r",0)
                        .style("opacity",0.7)
                        .style("stroke","#AAA")
                        .attr("class","circleDraw bicing")
                         .style("fill", function(d, j) {
                                        return (self.scaleType("bicing"));
                                    });

                        self.points.append("title").text(function(d,j){return self.dataPoints[j].data.stationName+" - "+self.dataPoints[j].data.slots.free+" plazas libres";});

                        self.points.transition().duration(self.transTime)
                        .attr("r", function(d,j){var scale= self.sizeScale['bicing'];var point = self.dataPoints[j]; return Math.floor(scale(point.data.slots.occupation))});


                }
            }
            console.log(self.drawPoints.length);

        }

        if(self.dataIn=='twitter' || self.dataIn=='all')
        {

            console.log("TWITTER");

            self.drawPoints = [];
            self.dataPoints = [];


            for(var i in self.data)
            {
                var point = self.data[i];

                if(point.type=='twitter')
                {
                    console.log(point);

                    self.drawPoints.push(self.projection([point.geo.info.lng, point.geo.info.lat]));
                    self.dataPoints.push(point);


                    self.points = self.svg.selectAll("circle.twitter")
                                .data(self.dataPoints)
                                .enter().append("svg:circle")
                                    .attr("cx", function(d,j) {
                                            return Math.floor(self.drawPoints[j][0]);
                                    })
                                    .attr("cy", function(d,j) {

                                            return Math.floor(self.drawPoints[j][1]);
                                    })
                        .attr("r",0)
                        .style("opacity",0.7)
                        .style("stroke","#AAA")
                        .attr("class","circleDraw twitter")
                         .style("fill", function(d, j) {
                                        return (self.scaleType("twitter"));
                                    });

                        self.points.append("title").text(function(d,j){return "@"+self.dataPoints[j].data.from_user+" - "+self.dataPoints[j].data.text;});

                        self.points.transition().duration(self.transTime)
                        .attr("r", 5);


                }
            }
            console.log(self.drawPoints.length);

        }




        if(self.dataIn=='foursquare' || self.dataIn=='all')
        {
            console.log("FOURSQUARE");

            self.drawPoints = [];
            self.dataPoints = [];


            for(var i in self.data)
            {
                var point = self.data[i];

            }

        }


        if(self.dataIn=='traffic' || self.dataIn=='all') {
            console.log("TRAFFIC");

            self.drawLines = [];
            self.dataLines = [];


            for(var i in self.data)
            {
                var point = self.data[i];


                if(point.type=='traffic')
                {
                    console.log("TRAFFIC");

                    var polygon = point.geo.info;

                    var polnumpoints = polygon.length-1;

                    for (var j=0;j<polnumpoints;j++)
                    {
                        self.dataLines.push({'first':self.projection([polygon[j][0], polygon[j][1]]),'second':self.projection([polygon[j+1][0], polygon[j+1][1]]),'now':point.data.now});
                    }

//                    self.drawPoints.push(self.projection([point.geo.info.lng, point.geo.info.lat]));
//                    self.dataPoints.push(point);
//
//                    self.points = self.svg.selectAll("g")
//                                .data(self.dataPoints)
//                                .enter().append("svg:circle")
//                                    .attr("cx", function(d,j) {
//                                            return self.drawPoints[j][0];
//                                    })
//                                    .attr("cy", function(d,j) {
//                                            return self.drawPoints[j][1];
//                                    })
//                        .attr("r",0)
//                        .attr("class","circleDraw")
//                        .style("opacity",0)
//                         .attr("fill", function(d, j) {
//                                        return (self.scaleType("foursquare"));
//                                    });
//
//                                        self.points.append("title").text(function(d,j){return self.dataPoints[j].data.name + " - " + self.dataPoints[j].data.checkins+" checkins"});
//
//                        self.points.transition().duration(self.transTime)
//                        .style("opacity",1)
//                        .attr("r", function(d,j){var scale= self.sizeScale['foursquare'];var point = self.dataPoints[j]; return scale(point.data.checkins)});
//

                }
            }

            //console.log(self.dataLines);


            self.lines = self.svg.selectAll("g")
                                .data(self.dataLines)
                                .enter().append("svg:line")
                                    .attr("x1", function(d,j) {
                                            return d.first[0];
                                    })
                                    .attr("y1", function(d,j) {
                                            return d.first[1];
                                    })
                                    .attr("x2", function(d,j) {
                                            return d.second[0];
                                    })
                                    .attr("y2", function(d,j) {
                                            return d.second[1];
                                    })
                        .attr("class","lineDraw")
                .attr("stroke",self.scaleType("traffic"))
                .attr("stroke-width",0)
                        .style("opacity",function(d,k){return self.sizeScale['traffic'](d.now);}).transition().duration(self.transTime).attr("stroke-width",function(d,k){return self.sizeScale['trafficW'](d.now);});


        }

        // El remove del warning esta al final porque el primer render tarda...

        self.warningMessage.transition().duration(self.transTime).style("opacity",0.0).remove();

    }

    // Main del objeto

    self.init();

    return self;

}

